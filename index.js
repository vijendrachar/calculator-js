let theTextView = document.getElementById('textview');

const insertNum = (char) => {
  theTextView.value = theTextView.value + char;
};

const equals = () => {
  if (theTextView.value) theTextView.value = eval(theTextView.value);
};

const clearBox = () => {
  theTextView.value = '';
};

const backspace = () => {
  let str = theTextView.value;
  theTextView.value = str.substring(0, str.length - 1);
};
